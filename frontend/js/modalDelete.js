// modal functions for delete
const modalDelete = document.querySelector(".modal-delete");
const triggerDelete = document.querySelectorAll(".delete-button");
const closeButton = document.querySelector(".close-button");

function toggleModal() {
  modalDelete.classList.toggle("show-modal");
}

function windowOnClick(event) {
  if (event.target === modalDelete) {
    toggleModal();
  }
}

closeButton.addEventListener("click", toggleModal);
window.addEventListener("click", windowOnClick);

Array.from(triggerDelete).forEach((trigger) => {
  trigger.addEventListener("click", function (event) {
    toggleModal();
  });
});
