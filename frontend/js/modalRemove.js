// modal functions for remove
const modalRemove = document.querySelector(".modal-remove");
const triggerRemove = document.querySelectorAll(".remove-button");

const closeButton = document.querySelector(".close-button");

function toggleModal() {
  modalRemove.classList.toggle("show-modal");
}

function windowOnClick(event) {
  if (event.target === modalRemove) {
    toggleModal();
  }
}

closeButton.addEventListener("click", toggleModal);
window.addEventListener("click", windowOnClick);

Array.from(triggerRemove).forEach((trigger) => {
  trigger.addEventListener("click", function (event) {
    toggleModal();
  });
});
