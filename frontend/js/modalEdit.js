// modal functions for delete
const modalEdit = document.querySelector(".modal-edit");
const triggerEdit = document.querySelectorAll(".edit-button");
const closeButtonEdit = document.querySelector(".close-button-edit");

function toggleModalEdit() {
  modalEdit.classList.toggle("show-modal");
}

function windowOnClick(event) {
  if (event.target === modalEdit) {
    toggleModalEdit();
  }
}

closeButtonEdit.addEventListener("click", toggleModalEdit);
window.addEventListener("click", windowOnClick);

Array.from(triggerEdit).forEach((trigger) => {
  trigger.addEventListener("click", function (event) {
    toggleModalEdit();
  });
});
